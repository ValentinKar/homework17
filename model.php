<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
require __DIR__ . '/vendor/autoload.php';   // подключение через composer (к twig)

function Connect()       // соединение с базой данных
{
    $host = 'localhost'; 
    $user = 'karpayev';      
    $password = 'neto1052';  
    $database = 'karpayev';    
    $dbport = 3306; 

      $pdo = new PDO("mysql:host=$host;dbname=$database;charset=utf8", $user, $password, [
          PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
        ]); 

return $pdo; 
}

function editTask( $id_task, $description_task, $id_user )      
{   // функция редактирования мероприятия в таблице task
$pdo = Connect();

  $id = (integer) $id_task;  
  $description = (string) $description_task;  
  $statement = $pdo->prepare("UPDATE task SET description = ? WHERE id = ?;"); 
  $statement->execute( ["{$description}", "{$id}"] );  

  if ( isset($id_user) && $id_user !== 'disabled' ) {    // если изменен ответственный за мероприятие
    $user = (integer) $id_user; 
    $statement = $pdo->prepare("UPDATE task SET assigned_user_id = ? WHERE id = ?;"); 
    $statement->execute( ["{$user}", "{$id}"] );  
  }; 
}; 

function pageEdit($id_task)    // функция вывода данных на страницу edit.html
{ 
$pdo = Connect(); 
$id = (integer) $id_task;
$statement = $pdo->prepare("SELECT 
  t.id,              -- id мероприятия 
  u.login AS assigned  -- логин ответственного за мероприятие 
   FROM task AS t  
   JOIN user AS u ON u.id=t.assigned_user_id 
   HAVING t.id = ? 
   ;"); 
$statement->execute( ["{$id}"] ); 
foreach ($statement as $row) { 
  $login = htmlspecialchars($row['assigned']); 
}; 
      $statement = $pdo->prepare("SELECT id, description FROM task WHERE id LIKE ?;"); 
      $statement->execute( ["{$id}"] ); 
      foreach ($statement as $row) { 
        $description = htmlspecialchars($row['description']); 
      }; 
$users = $pdo->prepare("SELECT id, login FROM user;"); 
$users->execute(); 
$users_arr = []; 
foreach ($users as $user) :  
  $users_arr[] = [
  'id' => htmlspecialchars( $user['id'] ), 
  'login' => htmlspecialchars( $user['login'] )
]; 
endforeach; 
    $page_edit = [ 
    'id' => $id, 
    'login' => $login, 
    'description' => $description, 
    'users' => $users_arr 
    ]; 
return $page_edit; 
}; 

function newTask( $description_task, $assigned_user ) 
{  // функция добавления мероприятия в таблицу task
if ( !isset($_SESSION['id_user']) ) { session_start(); }
$user = (integer) $_SESSION['id_user']; 
$description = (string) $description_task; 
$assigned = (integer) $assigned_user; 
      $statement = Connect()->prepare( "INSERT INTO task (user_id, assigned_user_id, description, is_done, date_added)
      VALUES ( ?, ?, ?, 1, now() );" ); 
      $statement->execute( ["{$user}", "{$assigned}", "{$description}"] ); 
}; 

function deleteTask($id_task) 
{   // функция удаления мероприятие в таблице task
  $id = (integer) $id_task;
  $statement = Connect()->prepare("DELETE FROM task WHERE id=?;");  
  $statement->execute( ["{$id}"] );   
}; 

function doneTask($id_task) 
{   // функция выполнения мероприятия в таблице task
  $id = (integer) $id_task;
  $statement = Connect()->prepare("UPDATE task SET is_done = 2 WHERE id = ?;"); 
  $statement->execute( ["{$id}"] );  
}; 

function allUserTasks()       // извлечение данных из таблицы task
{ 
$pdo = Connect(); 
if ( !isset($_SESSION['id_user']) ) { session_start(); }
$id_user = $_SESSION['id_user']; 
$statement = $pdo->prepare("SELECT id, login FROM user WHERE id LIKE ?;"); 
$statement->execute( [ (integer) "{$id_user}" ] ); 
foreach ($statement as $row) { 
$login = $row['login']; 
}; 
        $users = $pdo->prepare("SELECT * FROM user;"); 
        $users->execute(); 

        $users_arr = [];
        foreach ($users as $user) :  
            $users_arr[] = [
            'id' => htmlspecialchars( $user['id'] ), 
            'login' => htmlspecialchars( $user['login'] )
            ]; 
        endforeach; 
$statement = $pdo->prepare("SELECT 
  t.id,            -- id мероприятия
  t.description,    -- описание мероприятия
  t.date_added,     -- дата добавления мероприятия
  t.is_done,           -- выполнено мероприятие или нет
  u2.login AS assigned,  -- login ответственного за мероприятие
  u1.login AS autors   -- login автора мероприятия

   FROM task AS t  
   LEFT JOIN user AS u1 ON u1.id=t.user_id 
   LEFT JOIN user AS u2 ON u2.id=t.assigned_user_id   
   HAVING autors = ? 
   ;"); 
$statement->execute( ["{$login}"] ); 

$tasks_arr = []; 
foreach ( $statement as $row )  :  
    switch( $row['is_done'] )  
    {
        case 1: 
            $status = 'не выполнено'; 
            break;
        case 2: 
            $status = 'выполнено'; 
            break; 
        default: 
            $status = 'странный статус'; 
            break; 
    }; 
    $tasks_arr[] = [
    'autors' => htmlspecialchars( $row['autors'] ), 
    'assigned' => htmlspecialchars( $row['assigned'] ), 
    'description' => htmlspecialchars( $row['description'] ), 
    'date_added' => $row['date_added'], 
    'status' => $status, 
    'id' => $row['id'] 
    ]; 
endforeach; 

$user_tasks_arr = []; 
$statement = $pdo->prepare("SELECT t.id, 
  t.description,      -- описание мероприятия
  t.date_added,       -- дата добавления мероприятия
  t.is_done,           -- выполнено мероприятие или нет
  u1.login AS autors,   -- login автора мероприятия
  u2.login AS assigned   -- login ответственного за мероприятие

   FROM task AS t  
   LEFT JOIN user AS u1 ON u1.id=t.user_id 
   LEFT JOIN user AS u2 ON u2.id=t.assigned_user_id 
   HAVING assigned = ? 
   ;"); 
$statement->execute( ["{$login}"] ); 
foreach ( $statement as $row )  :  
    switch( $row['is_done'] )  
    {
        case 1: 
            $status = 'не выполнено'; 
            break;
        case 2: 
            $status = 'выполнено'; 
            break; 
        default: 
            $status = 'странный статус'; 
            break; 
    }; 
    $user_tasks_arr[] = [
    'assigned' => htmlspecialchars( $row['assigned'] ), 
    'autors' => htmlspecialchars( $row['autors'] ), 
    'description' => htmlspecialchars( $row['description'] ), 
    'date_added' => $row['date_added'], 
    'status' => $status, 
    ]; 
endforeach; 
$all_user_tasks = [ 
'login' => $login, 
'users' => $users_arr, 
'tasks' => $tasks_arr, 
'user_tasks' => $user_tasks_arr 
]; 
return $all_user_tasks; 
}; 

function isNewUser( $login_new, $password_new )        // функция добавления нового пользователя
{ 
$login = (string) $login_new; 
$password = md5( (string) $password_new  . getSalt() ); 
$pdo = Connect();  // соединяюсь с базой данных
$sth= $pdo->prepare("SELECT login FROM user WHERE login=:login;"); 
$sth->bindValue(':login', $login, PDO::PARAM_STR);
$sth->execute();
$result = $sth->fetch(PDO::FETCH_ASSOC);
if ( $result ) {    // проверяю уникальнось введеного login-а пользователя
    return ('пользователь с именем ' . $result["login"] . ' уже есть в базе данных'); 
}; 
    $statement = $pdo->prepare( "INSERT INTO user (login, password) 
    VALUES ( ?, ? );" );       // добавляю нового пользователя в таблицу базы данных
    $statement->execute( ["{$login}", "{$password}"] );  
$statement = $pdo->lastInsertId(); 
session_start(); 
$_SESSION['id_user'] = $statement;
return true; 
}; 

function isLogIndex( $login, $password )   // функция для авторизации пользователя
{
$statement = Connect()->prepare("SELECT * FROM user;"); 
$statement->execute(); 
    foreach ( $statement as $user ) { 
        if ( (string)$login === $user['login'] && md5( (string)$password . getSalt() ) === $user['password'] ) 
        { 
        session_start(); 
        $_SESSION['id_user'] = $user['id'];
        return true; 
        }; 
    }; 
return false;
}; 

function getSalt() 
{
    return 'ibtfnwhcpq';
};