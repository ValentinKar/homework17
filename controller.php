<?php 
error_reporting(E_ALL);        //вывести на экран все ошибки
require_once __DIR__ . '/vendor/autoload.php';   // подключение через composer (к twig)
require_once __DIR__ . '/model.php';   // 


function isActionGet()    // функция проверки, что надо вносить изменения в таблицу с мероприятиями
{ 
    return $_SERVER['REQUEST_METHOD'] == 'GET' && !empty($_GET['action']);
}; 

function isActionPost()    // функция проверки, что надо вносить изменения в таблицу с мероприятиями
{ 
    return $_SERVER['REQUEST_METHOD'] == 'POST' && !empty($_POST['action']);
}; 

function outputAuthorization($twig) 
{
    $template = $twig->loadTemplate('authorization.html');  
    $params = []; 
    $template->display($params); 
};

function outputRegistration($twig) 
{
    $template = $twig->loadTemplate('registration.html');  
    $params = []; 
    $template->display($params); 
};

function outputTask($twig) 
{ 
$template = $twig->loadTemplate('task.html'); 
$template->display( allUserTasks() ); 
}; 

function outputEdit( $twig, $id ) 
{ 
$template = $twig->loadTemplate('Edit.html'); 
    $template->display( pageEdit($id) ); 
}; 

// --- использую шаблонизатор twig --- 
$loader = new Twig_Loader_Filesystem('./templates');    // Где лежат шаблоны

$twig = new Twig_Environment($loader, [ 
    'cache' => './tmp/cache', 
    'auto_reload' => false   // true  // !!!!--- ПОСЛЕ ОТЛАДКИ ПОМЕНЯТЬ ---!!!!
]);   // Где будут хранится файлы кэша (php файлы)

if ( isActionPost() && $_POST['action'] == 'edit' && isset($_POST['id']) )  { // редактируется мероприятие в таблице task
    $_POST['descript_new'] = isset($_POST['descript_new']) ? $_POST['descript_new'] : ''; 
    editTask( $_POST['id'], $_POST['descript_new'], $_POST['users'] );   
    outputTask( $twig );
    die; 
}; 

if ( isActionGet() && $_GET['action'] == 'edit' && isset($_GET['id']) ) { 
   // редирект на страницу редактирования мероприятия в таблице task
    outputEdit( $twig, $_GET['id'] ); 
    die; 
}; 

if ( isActionPost() && $_POST['action'] == 'task' && isset($_POST['description']) && $_POST['users'] !== "disabled" )  { // добавляется новое мероприятие в таблицу task
    newTask( $_POST['description'], $_POST['users'] ); 
    outputTask( $twig );
    die; 
}; 

if ( isActionGet() && $_GET['action'] == 'delete' && isset($_GET['id']) ) { 
   // удалить мероприятие в таблице task
    deleteTask( $_GET['id'] ); 
    outputTask( $twig );
    die; 
}; 


if ( isActionGet() && $_GET['action'] == 'done' && isset($_GET['id']) ) { 
   // отметить мероприятие в таблице task как выполненное
    doneTask( $_GET['id'] ); 
    outputTask( $twig );
    die; 
}; 

if ( isActionPost() && $_POST['action'] == 'registration' && !empty($_POST['login_new']) ) {
$_POST['password_new'] = isset($_POST['password_new']) ? $_POST['password_new'] : ''; 
// регистрация нового пользователя
$new_user = isNewUser($_POST['login_new'], $_POST['password_new']);
    if ( $new_user === true ) {
        outputTask( $twig );
        die; 
    } 
    else { 
        echo $new_user; 
        outputRegistration($twig); 
        die; 
    }; 
}; 

if ( isActionPost() && $_POST['action'] == 'authorization' && isset($_POST['login']) ) {
$_POST['password'] = isset($_POST['password']) ? $_POST['password'] : ''; 
// авторизация пользователя
    if ( isLogIndex($_POST['login'], $_POST['password']) ) { 
        outputTask( $twig );
        die; 
    } 
    else { 
        outputRegistration($twig); 
        die; 
    }; 
}; 

if ( isActionGet() && $_GET['action'] == 'registry'  ) { 
    // редирект на страницу регистрации со страницы авторизации
    outputRegistration($twig); 
    die; 
}; 

outputAuthorization($twig); 
die; 